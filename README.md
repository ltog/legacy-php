# Legacy PHP

Information regarding working with legacy PHP (versions 4 and 5)

Special interest in versions

- 4.4.9
- 5.2.10

Note on .chm files: Under Ubuntu you can use `xchm` to open these files.

Links:

- [Zend PHP4 online manual](https://php-legacy-docs.zend.com/manual/php4/en/index)
- [Zend PHP5 online manual](https://php-legacy-docs.zend.com/manual/php5/en/index)
